Source: golang-go.uber-zap
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Alexandre Viau <aviau@debian.org>, Nilesh Patra <npatra974@gmail.com>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-pkg-errors-dev,
               golang-github-stretchr-testify-dev,
               golang-go.uber-atomic-dev,
               golang-go.uber-multierr-dev
Standards-Version: 4.5.0
Homepage: https://github.com/uber-go/zap
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-go.uber-zap
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-go.uber-zap.git
XS-Go-Import-Path: go.uber.org/zap
Testsuite: autopkgtest-pkg-go
Rules-Requires-Root: no

Package: golang-go.uber-zap-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-apex-log-dev,
         golang-github-pkg-errors-dev,
         golang-github-stretchr-testify-dev,
         golang-go.uber-atomic-dev,
         golang-go.uber-multierr-dev
Description: Blazing fast, structured, leveled logging in Go
 Package zap provides fast, structured, leveled logging.
 .
 For applications that log in the hot path, reflection-based
 serialization and string formatting are prohibitively expensive,
 they're CPU-intensive and make many small allocations. Put
 differently, using json.Marshal and fmt.Fprintf to log tons of
 interface{} makes your application slow.
 .
 Zap takes a different approach. It includes a reflection-free,
 zero-allocation JSON encoder, and the base Logger strives to avoid
 serialization overhead and allocations wherever possible. By
 building the high-level SugaredLogger on that foundation, zap lets
 users choose when they need to count every allocation and when
 they'd prefer a more familiar, loosely typed API.
